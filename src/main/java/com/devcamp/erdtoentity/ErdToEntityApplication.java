package com.devcamp.erdtoentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErdToEntityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErdToEntityApplication.class, args);
	}

}
