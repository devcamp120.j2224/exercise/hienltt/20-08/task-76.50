package com.devcamp.erdtoentity.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name ="product_lines")
public class ProductLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "product_line", unique = true)
    private String productLine;
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "productLine")
    @JsonIgnore
    private List<Product> product;
    
    public ProductLine() {
    }

    public ProductLine(int id, String productLine, String description, List<Product> product) {
        this.id = id;
        this.productLine = productLine;
        this.description = description;
        this.product = product;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }
    
}
