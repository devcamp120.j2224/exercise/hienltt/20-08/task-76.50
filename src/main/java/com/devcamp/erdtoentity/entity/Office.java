package com.devcamp.erdtoentity.entity;

import javax.persistence.*;

@Entity
@Table(name = "offices")
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String city;

    private String phone;
    @Column(name ="address_line")
    private String addressLine;

    private String state;

    private String country;

    private String territory;

    public Office() {
    }

    public Office(int id, String city, String phone, String addressLine, String state, String country,
            String territoty) {
        this.id = id;
        this.city = city;
        this.phone = phone;
        this.addressLine = addressLine;
        this.state = state;
        this.country = country;
        this.territory = territoty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTerritoty() {
        return territory;
    }

    public void setTerritoty(String territoty) {
        this.territory = territoty;
    }

    
}
