package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Office;
import com.devcamp.erdtoentity.repository.IOfficeRepository;

@RestController
@CrossOrigin
public class OfficeController {
    @Autowired
    private IOfficeRepository iOfficeRepository;

    @GetMapping("/offices")
    public List<Office> getAllEmployee(){
        return iOfficeRepository.findAll();
    }

    @GetMapping("/offices/{officeId}")
    public ResponseEntity<Office> getByOfficeId(@PathVariable("officeId") int officeId){
        try {
            Optional<Office> checkOffice = iOfficeRepository.findById(officeId);
            if(checkOffice.isPresent()){
                return new ResponseEntity<Office>(checkOffice.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/offices")
    public ResponseEntity<Office> createNewOfice(@RequestBody Office newOffice){
        try {
            Office _office = new Office();
            _office.setCity(newOffice.getCity());
            _office.setPhone(newOffice.getPhone());
            _office.setAddressLine(newOffice.getAddressLine());
            _office.setState(newOffice.getState());
            _office.setCountry(newOffice.getCountry());
            _office.setTerritoty(newOffice.getTerritoty());
            return new ResponseEntity<Office>(iOfficeRepository.save(_office), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/offices/{officeId}")
    public ResponseEntity<Office> updateByOfficeId(@PathVariable("officeId") int officeId, @RequestBody Office office){
        try {
            Optional<Office> checkOffice = iOfficeRepository.findById(officeId);
            if(checkOffice.isPresent()){
                Office _office = checkOffice.get();
                _office.setCity(office.getCity());
                _office.setPhone(office.getPhone());
                _office.setAddressLine(office.getAddressLine());
                _office.setState(office.getState());
                _office.setCountry(office.getCountry());
                _office.setTerritoty(office.getTerritoty());
                return new ResponseEntity<Office>(iOfficeRepository.save(_office), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/offices/{officeId}")
    public ResponseEntity<Office> deleteByCustomerId(@PathVariable("officeId") int officeId){
        try {
            iOfficeRepository.deleteById(officeId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
