package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Product;
import com.devcamp.erdtoentity.entity.ProductLine;
import com.devcamp.erdtoentity.repository.IProductLineRepository;
import com.devcamp.erdtoentity.repository.IProductRepository;

@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    private IProductRepository iProductRepository;
    @Autowired
    private IProductLineRepository iProductLineRepository;

    @GetMapping("/products")
    public List<Product> getAllProduct(){
        return iProductRepository.findAll();
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity<Product> getByProductId(@PathVariable("productId") int productId){
        try {
            Optional<Product> check = iProductRepository.findById(productId);
            if(check.isPresent()){
                return new ResponseEntity<Product>(check.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/product-line/{product-line-id}/product")
    public ResponseEntity<Object> createNewOrder(@PathVariable("product-line-id") int id, @RequestBody Product newProduct){
        try {
            Optional<ProductLine> checkProductLine = iProductLineRepository.findById(id);
            if(checkProductLine.isPresent()){
                Product _product = new Product();
                _product.setProductCode(newProduct.getProductCode());
                _product.setProductName(newProduct.getProductName());
                _product.setProductDescription(newProduct.getProductDescription());
                _product.setProductScale(newProduct.getProductScale());
                _product.setProductVendor(newProduct.getProductVendor());
                _product.setQuantityInStock(newProduct.getQuantityInStock());
                _product.setBuyPrice(newProduct.getBuyPrice());
                _product.setProductLine(checkProductLine.get());
                return new ResponseEntity<Object>(iProductRepository.save(_product), HttpStatus.CREATED);
            } else {
                return ResponseEntity.unprocessableEntity().body("Không tìm thấy Product Line");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/products/{productId}")
    public ResponseEntity<Object> updateByProductId(@PathVariable("productId") int productId, @RequestBody Product product){
        try {
            Optional<Product> check = iProductRepository.findById(productId);
            if(check.isPresent()){
                Product _product = check.get();
                _product.setProductCode(product.getProductCode());
                _product.setProductName(product.getProductName());
                _product.setProductDescription(product.getProductDescription());
                _product.setProductScale(product.getProductScale());
                _product.setProductVendor(product.getProductVendor());
                _product.setQuantityInStock(product.getQuantityInStock());
                _product.setBuyPrice(product.getBuyPrice());
                return new ResponseEntity<Object>(iProductRepository.save(_product), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/products/{productId}")
    public ResponseEntity<Product> deleteByProductId(@PathVariable("productId") int productId){
        try {
            iProductRepository.deleteById(productId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
