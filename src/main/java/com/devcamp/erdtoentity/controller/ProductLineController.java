package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.ProductLine;
import com.devcamp.erdtoentity.repository.IProductLineRepository;

@RestController
@CrossOrigin
public class ProductLineController {
    @Autowired
    private IProductLineRepository iProductLineRepository;

    @GetMapping("/product-lines")
    public List<ProductLine> getAllProductLine(){
        return iProductLineRepository.findAll();
    }

    @GetMapping("/product-lines/{id}")
    public ResponseEntity<ProductLine> getByProductLineId(@PathVariable("id") int id){
        try {
            Optional<ProductLine> check = iProductLineRepository.findById(id);
            if(check.isPresent()){
                return new ResponseEntity<ProductLine>(check.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/product-lines")
    public ResponseEntity<ProductLine> createNewProductLine(@RequestBody ProductLine newProductLine){
        try {
            ProductLine _productLine = new ProductLine();
            _productLine.setProductLine(newProductLine.getProductLine());
            _productLine.setDescription(newProductLine.getDescription());
            return new ResponseEntity<ProductLine>(iProductLineRepository.save(_productLine), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/product-line/{id}")
    public ResponseEntity<ProductLine> updateByCusTomerId(@PathVariable("id") int id, @RequestBody ProductLine productLine){
        try {
            Optional<ProductLine> check = iProductLineRepository.findById(id);
            if(check.isPresent()){
                ProductLine _productLine = check.get();
                _productLine.setProductLine(productLine.getProductLine());
                _productLine.setDescription(productLine.getDescription());
                return new ResponseEntity<ProductLine>(iProductLineRepository.save(_productLine), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/product-line/{id}")
    public ResponseEntity<ProductLine> deleteByCustomerId(@PathVariable("id") int id){
        try {
            iProductLineRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
