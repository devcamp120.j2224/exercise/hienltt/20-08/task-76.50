package com.devcamp.erdtoentity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdtoentity.entity.Order;
import com.devcamp.erdtoentity.entity.OrderDetail;
import com.devcamp.erdtoentity.entity.Product;
import com.devcamp.erdtoentity.repository.IOrderDetailRepository;
import com.devcamp.erdtoentity.repository.IOrderRepository;
import com.devcamp.erdtoentity.repository.IProductRepository;

@RestController
@CrossOrigin
public class OrderDetailController {
    @Autowired
    private IOrderDetailRepository iOrderDetailRepository;
    @Autowired
    private IOrderRepository iOrderRepository;
    @Autowired
    private IProductRepository iProductRepository;

    @GetMapping("/order-details")
    public List<OrderDetail> getAllOrderDetail(){
        return iOrderDetailRepository.findAll();
    }

    @GetMapping("/order-details/{id}")
    public ResponseEntity<OrderDetail> getByOrderDetailId(@PathVariable("id") int id){
        try {
            Optional<OrderDetail> check = iOrderDetailRepository.findById(id);
            if(check.isPresent()){
                return new ResponseEntity<OrderDetail>(check.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/order-details")
    public ResponseEntity<Object> createNewOrderDetail(@RequestParam("orderId") int orderId, @RequestParam("productId") int productId, @RequestBody OrderDetail newOrderDetail){
        try {
            Optional<Order> checkOrder = iOrderRepository.findById(orderId);
            Optional<Product> checkProduct = iProductRepository.findById(productId);
            if(checkOrder.isPresent() && checkProduct.isPresent()){
                OrderDetail _orDetail = new OrderDetail();
                _orDetail.setPriceEach(newOrderDetail.getPriceEach());
                _orDetail.setQuantityOrder(newOrderDetail.getQuantityOrder());
                _orDetail.setOrder(checkOrder.get());
                _orDetail.setProduct(checkProduct.get());
                return new ResponseEntity<Object>(iOrderDetailRepository.save(_orDetail), HttpStatus.CREATED);
            } else if(checkOrder.isPresent() && !checkProduct.isPresent()){
                return ResponseEntity.unprocessableEntity().body("Không tìm thấy Product");
            } else {
                return ResponseEntity.unprocessableEntity().body("Không tìm thấy Order");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/order-details/{id}")
    public ResponseEntity<Object> updateByOrderDetailId(@PathVariable("id") int id, @RequestBody OrderDetail orderDetail){
        try {
            Optional<OrderDetail> check = iOrderDetailRepository.findById(id);
            if(check.isPresent()){
                OrderDetail _order = check.get();
                _order.setPriceEach(orderDetail.getPriceEach());
                _order.setQuantityOrder(orderDetail.getQuantityOrder());
                return new ResponseEntity<Object>(iOrderDetailRepository.save(_order), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/order-details/{id}")
    public ResponseEntity<Order> deleteByOrderDetailId(@PathVariable("id") int id){
        try {
            iOrderDetailRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
