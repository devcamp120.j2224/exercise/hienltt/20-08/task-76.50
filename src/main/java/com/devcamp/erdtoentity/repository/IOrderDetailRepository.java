package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail, Integer>{
    
}
