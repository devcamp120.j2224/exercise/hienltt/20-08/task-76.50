package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.Order;

public interface IOrderRepository extends JpaRepository<Order, Integer>{
    
}
