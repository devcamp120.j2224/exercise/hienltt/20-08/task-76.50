package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository<ProductLine, Integer>{
    
}
