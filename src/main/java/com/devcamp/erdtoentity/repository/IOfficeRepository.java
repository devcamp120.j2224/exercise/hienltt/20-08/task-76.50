package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.Office;

public interface IOfficeRepository  extends JpaRepository<Office, Integer>{
    
}
