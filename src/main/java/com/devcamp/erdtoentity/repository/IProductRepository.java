package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.Product;

public interface IProductRepository extends JpaRepository<Product, Integer>{
    
}
