package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Integer>{
    
}
