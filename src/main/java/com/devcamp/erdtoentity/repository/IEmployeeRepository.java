package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Integer>{
    
}
