package com.devcamp.erdtoentity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.erdtoentity.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer,Integer>{
    
}
